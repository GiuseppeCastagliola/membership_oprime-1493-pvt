package com.odigeo.membership;

import bean.test.BeanTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class AutorenewalTrackingTest extends BeanTest<AutorenewalTracking> {

    private static final String REQUESTER = "requester-with-very-large-identifier";
    private static final String REQUESTED_METHOD = "requested-method-with-very-large-identifier";
    private static final String VISIT_INFORMATION = "1234567890";
    private static final String MEMBERSHIP_ID = "12345";
    private Integer INTERFACE_ID = 4;

    @Test
    public void testAutorenewalTrackingBuilder() {
        AutorenewalTracking autorenewalTracking = getBean();

        assertNotNull(autorenewalTracking.getTimestamp());
        assertEquals(autorenewalTracking.getRequestedMethod(), "REQUESTED-METHOD-WITH-VER");
        assertEquals(autorenewalTracking.getRequester(), "REQUESTER-WITH-VERY-LARGE");
    }

    protected AutorenewalTracking createAutorenewalTracking() {
        AutorenewalTracking autorenewalTracking = new AutorenewalTracking();
        autorenewalTracking.setRequestedMethod(REQUESTED_METHOD);
        autorenewalTracking.setRequester(REQUESTER);
        return autorenewalTracking;
    }

    @Override
    protected AutorenewalTracking getBean() {
        AutorenewalTracking autorenewalTracking = createAutorenewalTracking();
        autorenewalTracking.setInterfaceId(INTERFACE_ID);
        autorenewalTracking.setVisitInformation(VISIT_INFORMATION);
        autorenewalTracking.setAutoRenewalOperation(AutoRenewalOperation.ENABLE_AUTO_RENEW);
        autorenewalTracking.setMembershipId(MEMBERSHIP_ID);
        return autorenewalTracking;
    }
}