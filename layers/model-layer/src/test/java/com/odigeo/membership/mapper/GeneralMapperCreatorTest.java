package com.odigeo.membership.mapper;

import ma.glasnost.orika.MapperFacade;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class GeneralMapperCreatorTest {

    private GeneralMapperCreator generalMapperCreator;

    @BeforeMethod
    public void setUp() {
        this.generalMapperCreator = new GeneralMapperCreator();
    }

    @Test
    public void test() {
        MapperFacade mapperFacade = this.generalMapperCreator.getMapper();
        assertNotNull(this.generalMapperCreator.getMapperFactory());
        assertNotNull(mapperFacade);
        LocalDateTime nowDate = LocalDateTime.now();
        String now = mapperFacade.map(nowDate, String.class);
        LocalDateTime nowFromString = mapperFacade.map(now, LocalDateTime.class);
        assertEquals(nowDate, nowFromString);
    }
}
