package com.odigeo.membership.auth;


import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

public class PermissionResponseTest extends BeanTest<PermissionResponse> {
    @Test
    public void getPermissionResponseEqualsVerifierTest() {
        EqualsVerifier.forClass(PermissionResponse.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    @Override
    protected PermissionResponse getBean() {
        return assembleGetPermissionResponse();
    }

    private PermissionResponse assembleGetPermissionResponse() {
        PermissionResponse permissionResponse = new PermissionResponse();
        permissionResponse.setUserId("user");
        permissionResponse.setPerAppPermissions(new AppPermission());
        return permissionResponse;
    }
}