package com.odigeo.membership;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotNull;

public class MembershipRecurringTest extends BeanTest<MembershipRecurring> {

    private static final String RECURRING = "RECURRING01";
    private static final String STATUS = "SUCCESS";

    @Override
    protected MembershipRecurring getBean() {
        return MembershipRecurring.builder().build();
    }

    @Test
    public void testEquals() {
        EqualsVerifier.forClass(MembershipRecurring.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }

    @Test
    public void testHashCode() {
        MembershipRecurring membershipRecurring01 = MembershipRecurring.builder()
                .recurringId(RECURRING)
                .status(STATUS)
                .build();
        assertNotNull(membershipRecurring01.hashCode());
    }
}
