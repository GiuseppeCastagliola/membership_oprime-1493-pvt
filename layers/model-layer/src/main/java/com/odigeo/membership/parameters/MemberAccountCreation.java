package com.odigeo.membership.parameters;

public class MemberAccountCreation {
    private Long userId;
    private String name;
    private String lastNames;

    public MemberAccountCreation(MemberAccountCreationBuilder memberAccountCreationBuilder) {
        userId = memberAccountCreationBuilder.userId;
        name = memberAccountCreationBuilder.name;
        lastNames = memberAccountCreationBuilder.lastNames;
    }

    public static MemberAccountCreationBuilder builder() {
        return new MemberAccountCreationBuilder();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public static final class MemberAccountCreationBuilder {
        private Long userId;
        private String name;
        private String lastNames;

        public MemberAccountCreationBuilder userId(Long userId) {
            this.userId = userId;
            return this;
        }

        public MemberAccountCreationBuilder name(String name) {
            this.name = name;
            return this;
        }

        public MemberAccountCreationBuilder lastNames(String lastNames) {
            this.lastNames = lastNames;
            return this;
        }

        public MemberAccountCreation build() {
            return new MemberAccountCreation(this);
        }
    }
}
