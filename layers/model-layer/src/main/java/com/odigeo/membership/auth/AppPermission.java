package com.odigeo.membership.auth;

import java.util.Objects;
import java.util.Set;

public class AppPermission {
    private Set<ParametrizedPermission> permissions;

    public Set<ParametrizedPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(Set<ParametrizedPermission> permissions) {
        this.permissions = permissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AppPermission that = (AppPermission) o;
        return Objects.equals(permissions, that.permissions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(permissions);
    }
}
