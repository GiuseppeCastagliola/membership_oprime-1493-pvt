package com.odigeo.membership.parameters.search;

import java.math.BigDecimal;

public class MembershipSearchBuilder {

    protected String website;
    protected String status;
    protected String autoRenewal;
    protected String fromExpirationDate;
    protected String toExpirationDate;
    protected String fromActivationDate;
    protected String toActivationDate;
    protected String fromCreationDate;
    protected String toCreationDate;
    protected String membershipType;
    protected BigDecimal minBalance;
    protected BigDecimal maxBalance;
    protected Integer monthsDuration;
    protected String productStatus;
    protected BigDecimal totalPrice;
    protected BigDecimal renewalPrice;
    protected String currencyCode;
    protected String sourceType;
    protected Long memberAccountId;
    protected boolean withStatusActions;
    protected boolean withMemberAccount;
    protected MemberAccountSearch memberAccountSearch;

    public MembershipSearchBuilder website(String website) {
        this.website = website;
        return this;
    }

    public MembershipSearchBuilder status(String status) {
        this.status = status;
        return this;
    }

    public MembershipSearchBuilder autoRenewal(String autoRenewal) {
        this.autoRenewal = autoRenewal;
        return this;
    }

    public MembershipSearchBuilder fromExpirationDate(String fromExpirationDate) {
        this.fromExpirationDate = fromExpirationDate;
        return this;
    }

    public MembershipSearchBuilder toExpirationDate(String toExpirationDate) {
        this.toExpirationDate = toExpirationDate;
        return this;
    }

    public MembershipSearchBuilder fromActivationDate(String fromActivationDate) {
        this.fromActivationDate = fromActivationDate;
        return this;
    }

    public MembershipSearchBuilder toActivationDate(String toActivationDate) {
        this.toActivationDate = toActivationDate;
        return this;
    }

    public MembershipSearchBuilder fromCreationDate(String fromCreationDate) {
        this.fromCreationDate = fromCreationDate;
        return this;
    }

    public MembershipSearchBuilder toCreationDate(String toCreationDate) {
        this.toCreationDate = toCreationDate;
        return this;
    }

    public MembershipSearchBuilder membershipType(String membershipType) {
        this.membershipType = membershipType;
        return this;
    }

    public MembershipSearchBuilder monthsDuration(Integer monthsDuration) {
        this.monthsDuration = monthsDuration;
        return this;
    }

    public MembershipSearchBuilder productStatus(String productStatus) {
        this.productStatus = productStatus;
        return this;
    }

    public MembershipSearchBuilder totalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public MembershipSearchBuilder renewalPrice(BigDecimal renewalPrice) {
        this.renewalPrice = renewalPrice;
        return this;
    }

    public MembershipSearchBuilder currencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public MembershipSearchBuilder sourceType(String sourceType) {
        this.sourceType = sourceType;
        return this;
    }

    public MembershipSearchBuilder withStatusActions(boolean withStatusActions) {
        this.withStatusActions = withStatusActions;
        return this;
    }

    public MembershipSearchBuilder minBalance(BigDecimal minBalance) {
        this.minBalance = minBalance;
        return this;
    }

    public MembershipSearchBuilder maxBalance(BigDecimal maxBalance) {
        this.maxBalance = maxBalance;
        return this;
    }

    public MembershipSearchBuilder memberAccountId(Long memberAccountId) {
        this.memberAccountId = memberAccountId;
        return this;
    }

    public MembershipSearchBuilder withMemberAccount(boolean withMemberAccount) {
        this.withMemberAccount = withMemberAccount;
        return this;
    }

    public MembershipSearchBuilder memberAccountSearch(MemberAccountSearch memberAccountSearch) {
        this.memberAccountSearch = memberAccountSearch;
        return this;
    }

    public MembershipSearch build() {
        return new MembershipSearch(this);
    }
}


