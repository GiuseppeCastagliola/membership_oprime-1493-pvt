package com.odigeo.membership.util;

import com.edreams.configuration.ConfigurationEngine;
import com.edreams.util.string.transformer.StringTransformerChain;
import com.edreams.util.string.transformer.StringTransformerParametersFactory;
import com.edreams.util.string.transformer.impl.RemoveNoAlphaNumericsTransformer;
import com.edreams.util.string.transformer.impl.ReplaceNewLineOrTabToWhiteSpaceTransformer;
import com.google.inject.Singleton;

import java.util.Locale;

@Singleton
public class StringUtils {

    private static final String UTF8_BOM_UNICODE_REPRESENTATION = "\uFEFF";

    public String normalizeString(String str) {
        if (str == null) {
            return null;
        }
        return org.apache.commons.lang.StringUtils.normalizeSpace(str.toUpperCase(Locale.getDefault()));
    }

    public String concatenateString(String str1, String str2) {
        if (str1 == null) {
            return  str2;
        } else if (str2 == null) {
            return str1;
        } else {
            return str1 + str2;
        }
    }

    public String normalizeAndRemoveNonAlphaFrom(String str) {
        String result = normalizeString(str);
        result = removeNonAlphanumericsFrom(result);
        return result;
    }

    public String removeNonAlphanumericsFrom(String str) {
        return new StringTransformerChain(RemoveNoAlphaNumericsTransformer.class).transform(StringTransformerParametersFactory.getEmptyParameters(), str);
    }

    public String replaceNewLineOrTabByWhiteSpace(String text) {
        ReplaceNewLineOrTabToWhiteSpaceTransformer transformer = ConfigurationEngine.getInstance(ReplaceNewLineOrTabToWhiteSpaceTransformer.class);
        return transformer.transform(text, StringTransformerParametersFactory.getEmptyParameters());
    }

    public String removeUTF8BomHeader(String text) {
        if (text != null && text.startsWith(UTF8_BOM_UNICODE_REPRESENTATION)) {
            return text.replace(UTF8_BOM_UNICODE_REPRESENTATION, org.apache.commons.lang.StringUtils.EMPTY);
        }
        return text;
    }
}
