package com.odigeo.membership;

import java.util.Date;
import java.util.Objects;

public class BlacklistedPaymentMethod {

    private Long id;
    private Date timestamp;
    private String errorType;
    private String errorMessage;
    private Long membershipId;

    public Long getMembershipId() {
        return membershipId;
    }

    public void setMembershipId(Long membershipId) {
        this.membershipId = membershipId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp != null ? new Date(timestamp.getTime()) : null;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = new Date(timestamp.getTime());
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BlacklistedPaymentMethod that = (BlacklistedPaymentMethod) o;
        boolean isEqual = Objects.equals(id, that.id) && Objects.equals(timestamp, that.timestamp) && Objects.equals(errorType, that.errorType);
        isEqual &= Objects.equals(errorMessage, that.errorMessage) && Objects.equals(membershipId, that.membershipId);
        return isEqual;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, timestamp, errorType, errorMessage, membershipId);
    }
}
