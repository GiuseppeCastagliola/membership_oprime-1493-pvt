package com.odigeo.membership.auth;

public abstract class AuthServiceRequest {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
