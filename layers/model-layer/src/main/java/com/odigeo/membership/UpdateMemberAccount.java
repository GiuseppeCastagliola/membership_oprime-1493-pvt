package com.odigeo.membership;

public class UpdateMemberAccount {
    private String memberAccountId;
    private String operation;
    private String name;
    private String lastNames;
    private Long userId;

    public UpdateMemberAccount(String memberAccountId, String name, String lastNames, Long userId) {
        this.memberAccountId = memberAccountId;
        this.name = name;
        this.lastNames = lastNames;
        this.userId = userId;
    }

    public UpdateMemberAccount() {
    }

    public String getMemberAccountId() {
        return memberAccountId;
    }

    public void setMemberAccountId(String memberAccountId) {
        this.memberAccountId = memberAccountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOperation() {
        return operation;
    }

    public UpdateMemberAccount setOperation(String operation) {
        this.operation = operation;
        return this;
    }
}
