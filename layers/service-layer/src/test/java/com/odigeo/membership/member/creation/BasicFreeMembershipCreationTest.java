package com.odigeo.membership.member.creation;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.member.MemberManager;
import com.odigeo.membership.member.MemberStatusActionStore;
import com.odigeo.membership.parameters.MemberAccountCreation;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.parameters.MembershipCreationBuilder;
import com.odigeo.membership.parameters.UserCreation;
import com.odigeo.membership.parameters.search.MembershipSearch;
import com.odigeo.membership.search.SearchService;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.messaging.MembershipMessageSendingService;
import com.odigeo.userapi.UserApiManager;
import com.odigeo.userapi.UserInfo;
import com.odigeo.userprofiles.api.v1.model.Brand;
import com.odigeo.userprofiles.api.v1.model.UserBasicInfo;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Locale;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class BasicFreeMembershipCreationTest {
    private static final Long MEMBERSHIP_ID = 1L;
    private static final Long USER_ID = 1L;
    private static final String WEBSITE = "ES";
    private static final String EMAIL = "oprime@mail.com";
    @Mock
    private DataSource dataSource;
    @Mock
    private MemberManager memberManager;
    @Mock
    private UserApiManager userApiManager;
    @Mock
    private SearchService searchService;
    @Mock
    private MembershipMessageSendingService membershipMessageSendingService;
    @Mock
    private MemberStatusActionStore memberStatusActionStore;

    private MembershipCreationBuilder membershipCreationBuilder = new MembershipCreationBuilder()
            .withWebsite(WEBSITE)
            .withMemberAccountCreationBuilder(MemberAccountCreation.builder().userId(USER_ID))
            .withMembershipType(MembershipType.BASIC_FREE)
            .withMemberStatus(MemberStatus.ACTIVATED)
            .withExpirationDate(LocalDateTime.MAX)
            .withActivationDate(LocalDateTime.now());
    private MembershipCreationFactory membershipCreationFactory;
    private UserInfo userInfo;


    @BeforeMethod
    public void setUp() {
        initMocks(this);
        ConfigurationEngine.init(this::configure);
        UserBasicInfo userBasicInfo = new UserBasicInfo();
        userBasicInfo.setEmail("a");
        userBasicInfo.setBrand(Brand.ED);
        userBasicInfo.setWebsite(WEBSITE);
        userInfo = UserInfo.fromUserBasicInfo(userBasicInfo);
        membershipCreationFactory = ConfigurationEngine.getInstance(MembershipCreationFactoryProvider.class)
                .getInstance(membershipCreationBuilder.build());
    }

    private void configure(Binder binder) {
        binder.bind(MemberManager.class).toInstance(memberManager);
        binder.bind(UserApiManager.class).toInstance(userApiManager);
        binder.bind(SearchService.class).toInstance(searchService);
        binder.bind(MembershipMessageSendingService.class).toInstance(membershipMessageSendingService);
        binder.bind(MemberStatusActionStore.class).toInstance(memberStatusActionStore);
    }

    @Test
    public void checkRightFactoryReturnedTest() {
        assertTrue(membershipCreationFactory instanceof BasicFreeMembershipCreation);
    }

    @Test
    public void createMembershipWithExistingUser() throws MissingElementException, DataAccessException {
        //given
        MembershipCreation membershipCreation = membershipCreationBuilder.build();
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID);
        when(membershipMessageSendingService.sendMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class))).thenReturn(Boolean.TRUE);
        when(userApiManager.getUserInfo(eq(USER_ID))).thenReturn(userInfo);
        //when
        Long membershipId = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        //then
        assertEquals(membershipId, MEMBERSHIP_ID);
        verify(userApiManager).getUserInfo(eq(USER_ID));
    }

    @Test
    public void createMembershipWithExistingUserMissingEmail() throws MissingElementException, DataAccessException {
        //given
        MembershipCreation membershipCreation = membershipCreationBuilder.withUserCreation(new UserCreation.Builder().build()).build();
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID);
        when(membershipMessageSendingService.sendMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class))).thenReturn(Boolean.TRUE);
        when(userApiManager.getUserInfo(eq(USER_ID))).thenReturn(userInfo);
        //when
        Long membershipId = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        //then
        assertEquals(membershipId, MEMBERSHIP_ID);
        verify(userApiManager).getUserInfo(eq(USER_ID));
        verify(membershipMessageSendingService).sendMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class));
    }

    @Test
    public void createMembershipWithExistingUserUserProfileResponseWithoutEmail() throws MissingElementException, DataAccessException {
        //given
        MembershipCreation membershipCreation = membershipCreationBuilder.build();
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID);
        when(userApiManager.getUserInfo(eq(USER_ID))).thenReturn(UserInfo.defaultUserInfo());
        //when
        Long membershipId = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        //then
        assertEquals(membershipId, MEMBERSHIP_ID);
        verify(userApiManager).getUserInfo(eq(USER_ID));
        verify(membershipMessageSendingService, never()).sendMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class));
    }

    @Test
    public void createMembershipWithUserJustCreated() throws DataAccessException, MissingElementException {
        MembershipCreation membershipCreation = membershipCreationBuilder.withUserCreation(new UserCreation.Builder()
                .withTrafficInterfaceId(1)
                .withLocale(Locale.ENGLISH.toString())
                .withEmail(EMAIL).build())
                .build();
        when(memberManager.createMember(dataSource, membershipCreation)).thenReturn(MEMBERSHIP_ID);
        when(membershipMessageSendingService.sendMembershipSubscriptionMessage(any(MembershipSubscriptionMessage.class))).thenReturn(Boolean.TRUE);
        //when
        Long membershipId = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        //then
        assertEquals(membershipId, MEMBERSHIP_ID);
        verifyZeroInteractions(userApiManager);
    }

    @Test(expectedExceptions = DataAccessRollbackException.class)
    public void createMembershipFailBecauseAlreadyExistsActive() throws MissingElementException, DataAccessException {
        MembershipCreation membershipCreationUserId = new MembershipCreation(membershipCreationBuilder.withMemberAccountCreationBuilder(MemberAccountCreation.builder().userId(USER_ID)));
        //given
        when(searchService.searchMemberships(any(MembershipSearch.class))).thenReturn(Collections.singletonList(new MembershipBuilder().build()));
        //when
        membershipCreationFactory.createMembership(dataSource, membershipCreationUserId);
    }
}
