package com.odigeo.membership.member.update;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.UpdateMemberAccount;
import com.odigeo.membership.member.AuditMemberAccountManager;
import com.odigeo.membership.member.MemberAccountStore;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.sql.DataSource;
import java.security.InvalidParameterException;
import java.sql.SQLException;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertTrue;

public class MemberAccountUserIdUpdaterTest {

    private static final Long USER_ID = 123L;
    private static final Long MEMBER_ACCOUNT_ID = 1L;
    private static final UpdateMemberAccount UPDATE_MEMBER_ACCOUNT =
            new UpdateMemberAccount(String.valueOf(MEMBER_ACCOUNT_ID), "test", "test", USER_ID);

    private MemberAccountUserIdUpdater memberAccountUserIdUpdater;

    @Mock
    private DataSource dataSource;
    @Mock
    private MemberAccountStore memberAccountStore;
    @Mock
    private AuditMemberAccountManager auditMemberAccountManager;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        memberAccountUserIdUpdater = new MemberAccountUserIdUpdater(memberAccountStore, auditMemberAccountManager);
    }

    @Test
    public void testUpdateMemberUserIdAndAudit() throws SQLException, DataAccessException {
        when(memberAccountStore.updateMemberAccountUserId(eq(dataSource), anyLong(), anyLong())).thenReturn(true);
        assertTrue(memberAccountUserIdUpdater.updateMemberAccountUserId(dataSource, UPDATE_MEMBER_ACCOUNT));
        InOrder verifyOrder = inOrder(memberAccountStore, auditMemberAccountManager);
        verifyOrder.verify(memberAccountStore, times(1)).updateMemberAccountUserId(eq(dataSource), anyLong(), anyLong());
        verifyOrder.verify(auditMemberAccountManager).auditUpdatedMemberAccount(dataSource, MEMBER_ACCOUNT_ID);
    }

    @Test(expectedExceptions = DataAccessException.class)
    public void testUpdateMemberUserIdThrowsException() throws SQLException, DataAccessException {
        when(memberAccountStore.updateMemberAccountUserId(eq(dataSource), anyLong(), anyLong())).thenThrow(new SQLException());
        memberAccountUserIdUpdater.updateMemberAccountUserId(dataSource, UPDATE_MEMBER_ACCOUNT);
    }

    @Test(dataProvider = "notValidUpdateMemberAccountObjects", expectedExceptions = InvalidParameterException.class)
    public void testValidateParametersForUpdateUserIdOperation(UpdateMemberAccount updateMemberAccount) {
        memberAccountUserIdUpdater.validateParametersForUpdateUserIdOperation(updateMemberAccount);
    }

    @DataProvider(name = "notValidUpdateMemberAccountObjects")
    private static Object[][] notValidUpdateMemberAccountObjects() {
        return new Object[][]{
                new Object[]{new UpdateMemberAccount(String.valueOf(MEMBER_ACCOUNT_ID), "test", "test", null)},
                new Object[]{new UpdateMemberAccount(null, "test", "test", USER_ID)},
                new Object[]{new UpdateMemberAccount(null, "test", "test", null)}
        };
    }
}
