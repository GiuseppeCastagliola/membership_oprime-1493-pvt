package com.odigeo.messaging;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.crm.MemberStatusToCrmStatusMapper;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.message.MembershipMailerMessage;
import com.odigeo.membership.v4.messages.MembershipSubscriptionMessage;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import com.odigeo.userapi.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.odigeo.membership.message.enums.MessageType.WELCOME_TO_PRIME;
import static java.util.Objects.nonNull;

@Singleton
public class SubscriptionMessagePublisher {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionMessagePublisher.class);

    private final MembershipMessageSendingService membershipMessageSendingService;
    private final MembershipSubscriptionMessageService membershipSubscriptionMessageService;
    private final MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapper;

    @Inject
    public SubscriptionMessagePublisher(MembershipMessageSendingService membershipMessageSendingService,
                                        MembershipSubscriptionMessageService membershipSubscriptionMessageService,
                                        MemberStatusToCrmStatusMapper memberStatusToCrmStatusMapper) {
        this.membershipMessageSendingService = membershipMessageSendingService;
        this.membershipSubscriptionMessageService = membershipSubscriptionMessageService;
        this.memberStatusToCrmStatusMapper = memberStatusToCrmStatusMapper;
    }

    public void sendSubscriptionMessageToCRMTopicByRule(MemberStatus previousStatus, MemberStatus newStatus, Membership membership) {
        if (previousStatus.isNot(MemberStatus.PENDING_TO_ACTIVATE) && nonNull(membership.getExpirationDate())) {
            SubscriptionStatus subscriptionStatus = memberStatusToCrmStatusMapper.mapForUpdatedMembership(membership, newStatus);
            sendSubscriptionMessageToCRMTopic(membership, subscriptionStatus);
        }
    }

    public boolean sendSubscriptionMessageToCRMTopic(Membership membership, SubscriptionStatus subscriptionStatus) {
        LOGGER.info("Send subscription message for membershipId = {} with subscription status {}", membership.getId(), subscriptionStatus);
        try {
            final MembershipSubscriptionMessage membershipSubscriptionMessage = membershipSubscriptionMessageService
                    .getMembershipSubscriptionMessage(membership, subscriptionStatus);
            return membershipMessageSendingService.sendCompletedMembershipSubscriptionMessage(membershipSubscriptionMessage);
        } catch (DataNotFoundException e) {
            LOGGER.warn("Impossible to send subscription message with membershipId {}: {}", membership.getId(), e.getMessage());
            return false;
        }
    }

    public void sendSubscriptionMessageToCRMTopic(UserInfo userInfo, Membership membership, SubscriptionStatus subscriptionStatus) {
        LOGGER.info("Send subscription message for membershipId = {} with subscription status {}", membership.getId(), subscriptionStatus);
        try {
            final MembershipSubscriptionMessage membershipSubscriptionMessage = membershipSubscriptionMessageService
                    .buildMembershipSubscriptionMessage(userInfo, membership, subscriptionStatus);
            membershipMessageSendingService.sendCompletedMembershipSubscriptionMessage(membershipSubscriptionMessage);
        } catch (DataNotFoundException e) {
            LOGGER.warn("Impossible to send subscription message with membershipId {}: {}", membership.getId(), e.getMessage());
        }
    }

    public void sendWelcomeToPrimeMessageToMembershipTransactionalTopic(Membership membership, long bookingId, boolean force) {
        final MembershipMailerMessage membershipMailerMessage = new MembershipMailerMessage.Builder()
                .withMembershipId(membership.getId())
                .withUserId(membership.getMemberAccount().getUserId())
                .withBookingId(bookingId)
                .withMessageType(WELCOME_TO_PRIME)
                .build();
        membershipMessageSendingService.sendMembershipMailerMessage(membershipMailerMessage, force);
    }
}
