package com.odigeo.messaging.kafka;

import com.odigeo.messaging.utils.Consumer;
import com.odigeo.messaging.utils.Message;
import com.odigeo.messaging.utils.Publisher;
import com.odigeo.messaging.utils.kafka.KafkaJsonConsumer;
import com.odigeo.messaging.utils.kafka.KafkaJsonPublisher;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless
@Local(MessageIntegrationService.class)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class MessageIntegrationServiceBean<T extends Message> implements MessageIntegrationService<T> {

    @Override
    public Publisher<T> createKafkaPublisher(String url) {
        return new KafkaJsonPublisher.Builder<T>(url).build();
    }

    @Override
    public Consumer<T> createKafkaConsumer(String url, String groupId, String topic, Class<T> typeOfMessage) {
        return new KafkaJsonConsumer<>(typeOfMessage, url, groupId, topic);
    }
}
