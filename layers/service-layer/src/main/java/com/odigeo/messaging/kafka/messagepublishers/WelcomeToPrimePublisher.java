package com.odigeo.messaging.kafka.messagepublishers;

import com.google.inject.Singleton;
import com.odigeo.commons.messaging.KafkaPublisher;
import com.odigeo.commons.messaging.Message;
import com.odigeo.commons.messaging.MessagePublisher;
import com.odigeo.commons.messaging.PublishMessageException;
import com.odigeo.membership.message.MembershipMailerMessage;
import org.apache.kafka.clients.producer.Callback;
import org.apache.log4j.Logger;

import java.lang.reflect.UndeclaredThrowableException;

@Singleton
public class WelcomeToPrimePublisher {

    private static final Logger LOGGER = Logger.getLogger(WelcomeToPrimePublisher.class);

    @MessagePublisher(topic = "MEMBERSHIP_TRANSACTIONAL_ACTIVATIONS_v1")
    private KafkaPublisher<Message> publisher;

    public void publishMembershipMailerMessage(MembershipMailerMessage membershipMailerMessage, Callback callback) {
        try {
            publisher.publishMessage(membershipMailerMessage, callback);
            LOGGER.info(String.format("WelcomeToPrime message published - membershipId:%s, bookingId:%s",
                    membershipMailerMessage.getMembershipId(), membershipMailerMessage.getBookingId()));
        } catch (UndeclaredThrowableException | PublishMessageException e) {
            LOGGER.error(String.format("Error publishing WelcomeToPrime trigger message: membershipId:%s, bookingId:%s",
                    membershipMailerMessage.getMembershipId(), membershipMailerMessage.getBookingId()), e);
        }
    }
}
