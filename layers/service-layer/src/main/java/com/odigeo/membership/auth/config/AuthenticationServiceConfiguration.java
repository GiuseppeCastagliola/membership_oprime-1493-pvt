package com.odigeo.membership.auth.config;

import com.odigeo.authservices.authentication.api.v1.client.AuthServicesAuthenticationApiClientConfiguration;

public class AuthenticationServiceConfiguration extends AuthServicesAuthenticationApiClientConfiguration {

    public AuthenticationServiceConfiguration(final String url) {
        super();
        setUrl(url);
    }
}
