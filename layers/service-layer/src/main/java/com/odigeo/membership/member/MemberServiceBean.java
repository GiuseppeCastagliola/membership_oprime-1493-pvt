package com.odigeo.membership.member;

import com.edreams.base.DataAccessException;
import com.edreams.base.MissingElementException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.commons.monitoring.metrics.MetricsBuilder;
import com.odigeo.commons.monitoring.metrics.MetricsNames;
import com.odigeo.commons.monitoring.metrics.MetricsUtils;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.StatusAction;
import com.odigeo.membership.exception.DataAccessRollbackException;
import com.odigeo.membership.exception.DataNotFoundException;
import com.odigeo.membership.member.creation.MembershipCreationFactory;
import com.odigeo.membership.member.creation.MembershipCreationFactoryProvider;
import com.odigeo.membership.member.user.UserService;
import com.odigeo.membership.parameters.MembershipCreation;
import com.odigeo.membership.v4.messages.SubscriptionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.isNull;

@Stateless
@Local(MemberService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class MemberServiceBean extends AbstractServiceBean implements MemberService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MemberServiceBean.class);
    private MembershipCreationFactoryProvider membershipCreationFactoryProvider;
    private UserService userService;

    @Override
    public Membership getMembershipById(long membershipId) throws MissingElementException, DataAccessException {
        try {
            return getMembershipStore().fetchMembershipById(dataSource, membershipId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Member not found by id: " + membershipId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load member with id: " + membershipId, e);
        }
    }

    @Override
    public Membership getMembershipByIdWithMemberAccount(long membershipId) throws MissingElementException, DataAccessException {
        try {
            return getMembershipStore().fetchMembershipByIdWithMemberAccount(dataSource, membershipId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Member not found by id: " + membershipId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load member with id: " + membershipId, e);
        }
    }

    @Override
    public List<Membership> getMembershipsByAccountId(long memberAccountId) throws MissingElementException, DataAccessException {
        try {
            return getMembershipStore().fetchMembershipByMemberAccountId(dataSource, memberAccountId);
        } catch (DataNotFoundException e) {
            throw new MissingElementException("Memberships not found by memberAccountId: " + memberAccountId, e);
        } catch (SQLException e) {
            throw new DataAccessException("There was an error trying to load memberships with memberAccountId: " + memberAccountId, e);
        }
    }

    @Override
    public Boolean activateMembership(long membershipId, long bookingId, BigDecimal balance) throws DataAccessException {
        boolean result = false;
        try {
            Membership membership = Optional.ofNullable(getMembershipStore().fetchMembershipByIdWithMemberAccount(dataSource, membershipId))
                    .orElseThrow(() -> new MissingElementException("Membership id not found in database: " + membershipId));
            final boolean isMembershipPendingToActivate = MemberStatus.PENDING_TO_ACTIVATE.equals(membership.getStatus());
            final boolean doesNotHaveActiveMembershipForWebsite = validateUserDoesNotHaveActiveMembershipForWebsite(membership);
            if (isMembershipPendingToActivate && doesNotHaveActiveMembershipForWebsite) {
                LocalDateTime activationDate = LocalDateTime.now();
                result = getMembershipStore().activateMember(dataSource, membership.getId(), activationDate, activationDate.plusMonths(membership.getMonthsDuration()), balance);
                if (result) {
                    LOGGER.info("Membership activated for membershipId {}", membership.getId());
                    getMemberStatusActionStore().createMemberStatusAction(dataSource, membership.getId(), StatusAction.ACTIVATION);
                    getMembershipMessageSendingService().sendMembershipIdToMembershipReporter(membership.getId());
                    Membership activatedMembership = getMembershipStore().fetchMembershipByIdWithMemberAccount(dataSource, membershipId);
                    getSubscriptionMessagePublisher().sendSubscriptionMessageToCRMTopic(activatedMembership, SubscriptionStatus.SUBSCRIBED);
                    getSubscriptionMessagePublisher().sendWelcomeToPrimeMessageToMembershipTransactionalTopic(membership, bookingId, false);
                    MetricsUtils.incrementCounter(MetricsBuilder.buildMetric(MetricsNames.ACTIVATIONS_NUMBER), MetricsNames.METRICS_REGISTRY_NAME);
                }
            }
        } catch (SQLException | MissingElementException e) {
            throw new DataAccessRollbackException("Cannot activate membership for membershipId " + membershipId + " .Error Status Action ACTIVATION", e);
        }
        return result;
    }

    private boolean validateUserDoesNotHaveActiveMembershipForWebsite(Membership membership) throws DataAccessException {
        return getMemberAccountService().getActiveMembersByUserId(membership.getMemberAccount().getUserId()).stream()
                .filter(MemberServiceHelper.websiteCheck(membership.getWebsite()))
                .flatMap(memberAccount -> memberAccount.getMemberships().stream())
                .noneMatch(Membership::getIsActive);
    }

    @Override
    public Long createMembership(MembershipCreation membershipCreation) throws DataAccessException, MissingElementException {
        if (isNull(membershipCreation.getMemberAccountCreation().getUserId()) && isNull(membershipCreation.getMemberAccountId())) {
            membershipCreation.getMemberAccountCreation().setUserId(getUserService().saveUser(membershipCreation.getUserCreation()));
        }
        MembershipCreationFactory membershipCreationFactory = getMembershipCreationFactoryProvider().getInstance(membershipCreation);
        Long membershipId = membershipCreationFactory.createMembership(dataSource, membershipCreation);
        getMembershipMessageSendingService().sendMembershipIdToMembershipReporter(membershipId);
        return membershipId;
    }

    @Override
    public boolean existsMembership(long memberId) throws DataAccessException {
        try {
            return getMembershipStore().fetchMembershipById(dataSource, memberId, true).isPresent();
        } catch (DataNotFoundException | SQLException e) {
            throw new DataAccessException("There was an error trying to determine whether a membership with id " + memberId + " already exists", e);
        }
    }

    private MembershipCreationFactoryProvider getMembershipCreationFactoryProvider() {
        return Optional.ofNullable(membershipCreationFactoryProvider).orElseGet(() -> {
            membershipCreationFactoryProvider = ConfigurationEngine.getInstance(MembershipCreationFactoryProvider.class);
            return membershipCreationFactoryProvider;
        });
    }

    private UserService getUserService() {
        return Optional.ofNullable(userService).orElseGet(() -> {
            userService = ConfigurationEngine.getInstance(UserService.class);
            return userService;
        });
    }

    private MemberAccountService getMemberAccountService() {
        return ConfigurationEngine.getInstance(MemberAccountService.class);
    }
}
