package com.odigeo.membership.product;

import com.edreams.base.DataAccessException;
import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.BlacklistedPaymentMethod;
import com.odigeo.membership.member.AbstractServiceBean;
import com.odigeo.membership.member.PaymentMethodsBlacklistStore;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.List;

@Stateless
@Local(BlacklistPaymentService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class BlacklistPaymentServiceBean extends AbstractServiceBean implements BlacklistPaymentService {

    @Override
    public List<BlacklistedPaymentMethod> getBlacklistedPaymentMethods(Long membershipId) throws DataAccessException {
        return getPaymentMethodsBlacklistStore().fetchBlackListedPaymentMethods(dataSource, membershipId);
    }

    @Override
    public Boolean addToBlackList(List<BlacklistedPaymentMethod> blacklistedPaymentMethods) throws DataAccessException {
        return getPaymentMethodsBlacklistStore().addToBlackList(dataSource, blacklistedPaymentMethods);
    }

    private PaymentMethodsBlacklistStore getPaymentMethodsBlacklistStore() {
        return ConfigurationEngine.getInstance(PaymentMethodsBlacklistStore.class);
    }
}
