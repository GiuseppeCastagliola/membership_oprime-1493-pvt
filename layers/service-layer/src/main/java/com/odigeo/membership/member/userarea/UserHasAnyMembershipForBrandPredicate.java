package com.odigeo.membership.member.userarea;

import com.edreams.base.DataAccessException;
import com.google.common.collect.Maps;
import com.google.inject.Inject;
import com.odigeo.membership.member.MemberAccountService;

import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

class UserHasAnyMembershipForBrandPredicate {

    private static final String EDREAMS_BRAND_PREFIX = "ED";

    private final MemberAccountService memberAccountService;

    @Inject
    UserHasAnyMembershipForBrandPredicate(final MemberAccountService memberAccountService) {
        this.memberAccountService = memberAccountService;
    }

    boolean testAnyMembershipForBrand(final Long userId, final String brand) throws DataAccessException {
        Map<String, Set<String>> sitesByBrand = memberAccountService.getActiveMembershipSitesByUserId(userId).stream()
                .map(String::toUpperCase)
                .map(this::splitSiteByBrand)
                .collect(Collectors.groupingBy(Map.Entry::getKey, Collectors.mapping(Map.Entry::getValue, Collectors.toSet())));
        Set<String> brandSites = sitesByBrand.get(brand.toUpperCase(Locale.getDefault()));

        return nonNull(brandSites) && !brandSites.isEmpty();
    }

    private Map.Entry<String, String> splitSiteByBrand(final String website) {
        String trimmedWebsite = website.trim();
        if (siteHasNoPrefix(trimmedWebsite)) {
            return Maps.immutableEntry(EDREAMS_BRAND_PREFIX, trimmedWebsite);
        }
        return Maps.immutableEntry(brand(trimmedWebsite), site(trimmedWebsite));
    }

    private String brand(final String website) {
        return website.substring(0, 2);
    }

    private String site(final String website) {
        return website.substring(2);
    }

    private boolean siteHasNoPrefix(final String website) {
        return website.length() <= 2;
    }
}
