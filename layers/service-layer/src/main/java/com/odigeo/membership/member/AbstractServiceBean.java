package com.odigeo.membership.member;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.member.configuration.JeeResourceLocator;
import com.odigeo.membership.recurring.MembershipRecurringStore;
import com.odigeo.messaging.MembershipMessageSendingService;
import com.odigeo.messaging.SubscriptionMessagePublisher;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.Optional;

public abstract class AbstractServiceBean {

    @Resource(mappedName = JeeResourceLocator.DEFAULT_DATASOURCE_NAME)
    protected DataSource dataSource;

    private MembershipRecurringStore membershipRecurringStore;
    private MemberManager memberManager;
    private MembershipStore membershipStore;
    private MemberStatusActionStore memberStatusActionStore;
    private MemberAccountStore memberAccountStore;
    private SubscriptionMessagePublisher subscriptionMessagePublisher;
    private MembershipMessageSendingService membershipMessageSendingService;

    protected MemberManager getMemberManager() {
        return Optional.ofNullable(memberManager).orElseGet(() -> {
            memberManager = ConfigurationEngine.getInstance(MemberManager.class);
            return memberManager;
        });
    }

    protected MembershipRecurringStore getMembershipRecurringStore() {
        return Optional.ofNullable(membershipRecurringStore).orElseGet(() -> {
            membershipRecurringStore = ConfigurationEngine.getInstance(MembershipRecurringStore.class);
            return membershipRecurringStore;
        });
    }

    protected MembershipStore getMembershipStore() {
        return Optional.ofNullable(membershipStore).orElseGet(() -> {
            membershipStore = ConfigurationEngine.getInstance(MembershipStore.class);
            return membershipStore;
        });
    }

    protected MemberStatusActionStore getMemberStatusActionStore() {
        return Optional.ofNullable(memberStatusActionStore).orElseGet(() -> {
            memberStatusActionStore = ConfigurationEngine.getInstance(MemberStatusActionStore.class);
            return memberStatusActionStore;
        });
    }

    protected MemberAccountStore getMemberAccountStore() {
        return Optional.ofNullable(memberAccountStore).orElseGet(() -> {
            memberAccountStore = ConfigurationEngine.getInstance(MemberAccountStore.class);
            return memberAccountStore;
        });
    }

    protected MembershipMessageSendingService getMembershipMessageSendingService() {
        return Optional.ofNullable(membershipMessageSendingService).orElseGet(() -> {
            membershipMessageSendingService = ConfigurationEngine.getInstance(MembershipMessageSendingService.class);
            return membershipMessageSendingService;
        });
    }

    protected SubscriptionMessagePublisher getSubscriptionMessagePublisher() {
        return Optional.ofNullable(subscriptionMessagePublisher).orElseGet(() -> {
            subscriptionMessagePublisher = ConfigurationEngine.getInstance(SubscriptionMessagePublisher.class);
            return subscriptionMessagePublisher;
        });
    }
}
