package com.odigeo.membership.tracking;

import com.google.common.annotations.VisibleForTesting;
import com.google.inject.Inject;
import com.google.inject.servlet.RequestScoped;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RequestScoped
public class RequestContext {
    @Inject
    @VisibleForTesting
    HttpServletRequest httpServletRequest;

    public Optional<String> getHeader(String headerKey) {
        return Optional.ofNullable(httpServletRequest).map(r -> r.getHeader(headerKey));
    }

}
