package com.odigeo.membership.tracking;

import com.edreams.base.DataAccessException;
import com.odigeo.membership.BookingTracking;

public interface BookingTrackingService {

    boolean isBookingLimitReached(Long memberId) throws DataAccessException;

    BookingTracking getMembershipBookingTracking(long bookingId) throws DataAccessException;

    BookingTracking insertMembershipBookingTracking(BookingTracking bookingTracking) throws DataAccessException;

    void deleteMembershipBookingTracking(long bookingId) throws DataAccessException;
}
