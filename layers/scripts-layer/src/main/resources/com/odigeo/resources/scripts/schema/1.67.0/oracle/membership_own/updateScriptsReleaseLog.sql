UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.67.0' where ID ='UNL-5953/01.drop_membership_recurring_pk.ddl.sql';
UPDATE GE_SCRIPTS_LOG SET RELEASE_ID = '1.67.0' where ID ='UNL-5953/02.ge_membership_recurring_id_allows_null.ddl.sql';
INSERT INTO GE_RELEASE_LOG (ID,EXECUTION_DATE,MODULE) values('1.67.0',systimestamp,'membership');
