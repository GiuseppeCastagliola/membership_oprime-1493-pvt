package com.odigeo.membership.propertiesconfig;

public enum PropertiesConfigurationStoreKeys {
    SEND_IDS_TO_KAFKA,
    TRANSACTIONAL_EMAILS
}
