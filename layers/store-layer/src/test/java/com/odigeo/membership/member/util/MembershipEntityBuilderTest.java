package com.odigeo.membership.member.util;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.Binder;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.Membership;
import com.odigeo.membership.MembershipRecurring;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.enums.MembershipType;
import com.odigeo.membership.enums.SourceType;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class MembershipEntityBuilderTest {

    private static final Long ID = 123L;
    private static final String WEBSITE = "ES";
    private static final String STATUS = "ACTIVATED";
    private static final String AUTO_RENEWAL = "ENABLED";
    private static final long MEMBER_ACCOUNT_ID_1 = 333L;
    private static final Date utilDate = new Date();
    private static final Timestamp ACTIVATION_TIMESTAMP = new Timestamp(utilDate.getTime());
    private static final Timestamp EXPIRATION_TIMESTAMP = new Timestamp(utilDate.getTime());
    private static final BigDecimal BALANCE = BigDecimal.TEN;
    private static final String MEMBERSHIP_TYPE = MembershipType.BASIC.toString();
    private static final String SOURCE_TYPE = SourceType.FUNNEL_BOOKING.toString();
    private static final int MONTHS_DURATION = 12;
    private static final String PRODUCT_STATUS = null;
    private static final String RECURRING_ID_01 = "A1234556";
    private static final String RECURRING_ID_02 = "B1234556";
    private static final Timestamp MEMBERSHIP_TIMESTAMP = new Timestamp(utilDate.getTime());;
    private static final String CURRENCY_CODE = null;
    private static final BigDecimal TOTAL_PRICE = new BigDecimal(44.99);
    private static final BigDecimal RENEWAL_PRICE = new BigDecimal(45.99);
    private static final MembershipRecurring MEMBERSHIP_RECURRING_01 = MembershipRecurring.builder().recurringId(RECURRING_ID_01).build();
    private static final MembershipRecurring MEMBERSHIP_RECURRING_02 = MembershipRecurring.builder().recurringId(RECURRING_ID_02).build();


    @Mock
    private ResultSet resultSet;
    @Mock
    private MemberAccount memberAccount;
    @Mock
    private MemberAccountEntityBuilder memberAccountEntityBuilder;
    @Mock
    private MembershipRecurringEntityBuilder membershipRecurringEntityBuilder;

    private MembershipEntityBuilder membershipEntityBuilder;

    @BeforeMethod
    public void before() {
        initMocks(this);
        ConfigurationEngine.init(this::init);
        membershipEntityBuilder = new MembershipEntityBuilder();
    }

    @Test
    public void testBuildWithoutAccount() throws SQLException {
        // Given
        mockResultSet(true);
        // When
        Membership membership = membershipEntityBuilder.build(resultSet, false);
        // Then
        verifyMembershipFields(membership);
    }

    @Test
    public void testBuildWithAccount() throws SQLException {
        // Given
        mockResultSet(true);
        when(memberAccountEntityBuilder.build(resultSet, "MEMBER_ACCOUNT_ID")).thenReturn(memberAccount);
        // When
        Membership membership = membershipEntityBuilder.build(resultSet, true);
        // Then
        verifyMembershipFields(membership);
        assertEquals(membership.getMemberAccount(), memberAccount);
    }

    @Test
    public void testBuildListMembershipWithOneRecurringFromResultSet() throws SQLException {
        // Given
        mockResultSet(true);
        mockMembershipRecurringEntityBuilder();
        // When
        List<Membership> membershipList = membershipEntityBuilder.buildListFromResultSet(resultSet);
        // Then
        assertEquals(membershipList.size(), 1);
        assertEquals(membershipList.get(0).getMembershipRecurring(), Collections.singleton(MEMBERSHIP_RECURRING_01));
    }

    @Test
    public void testBuildListMembershipTwoRecurringFromResultSet() throws SQLException {
        // Given
        mockResultSet(false);
        mockMembershipRecurringEntityBuilder();
        // When
        List<Membership> membershipList = membershipEntityBuilder.buildListFromResultSet(resultSet);
        // Then
        assertEquals(membershipList.size(), 1);
        assertEquals(membershipList.get(0).getMembershipRecurring(), Arrays.asList(MEMBERSHIP_RECURRING_01, MEMBERSHIP_RECURRING_02));
    }

    private void verifyMembershipFields(Membership membership) {
        assertNotNull(membership);
        assertEquals(membership.getId(), ID);
        assertEquals(membership.getWebsite(), WEBSITE);
        assertEquals(membership.getStatus(), MemberStatus.valueOf(STATUS));
        assertEquals(membership.getAutoRenewal(), MembershipRenewal.valueOf(AUTO_RENEWAL));
        assertEquals(membership.getMemberAccountId(), MEMBER_ACCOUNT_ID_1);
        assertEquals(membership.getExpirationDate(), EXPIRATION_TIMESTAMP.toLocalDateTime());
        assertEquals(membership.getActivationDate(), ACTIVATION_TIMESTAMP.toLocalDateTime());
        assertEquals(membership.getBalance(), BALANCE);
        assertEquals(membership.getMembershipType(), MembershipType.valueOf(MEMBERSHIP_TYPE));
        assertEquals(membership.getSourceType(), SourceType.valueOf(SOURCE_TYPE));
        assertEquals(membership.getMonthsDuration(), MONTHS_DURATION);
        assertNull(membership.getProductStatus());
        assertEquals(membership.getRecurringId(), RECURRING_ID_01);
        assertEquals(membership.getTimestamp(), MEMBERSHIP_TIMESTAMP.toLocalDateTime());
        assertEquals(membership.getCurrencyCode(), CURRENCY_CODE);
        assertEquals(membership.getTotalPrice(), TOTAL_PRICE);
        assertEquals(membership.getRenewalPrice(), RENEWAL_PRICE);
    }

    private void mockResultSet(boolean hasMembershipOneRecurring) throws SQLException {
        if (hasMembershipOneRecurring) {
            when(resultSet.next()).thenReturn(true).thenReturn(false);
        } else {
            when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        }
        when(resultSet.getLong("ID")).thenReturn(ID);
        when(resultSet.getString("WEBSITE")).thenReturn(WEBSITE);
        when(resultSet.getString("STATUS")).thenReturn(STATUS);
        when(resultSet.getString("AUTO_RENEWAL")).thenReturn(AUTO_RENEWAL);
        when(resultSet.getLong("MEMBER_ACCOUNT_ID")).thenReturn(MEMBER_ACCOUNT_ID_1);
        when(resultSet.getTimestamp("EXPIRATION_DATE")).thenReturn(EXPIRATION_TIMESTAMP);
        when(resultSet.getTimestamp("ACTIVATION_DATE")).thenReturn(ACTIVATION_TIMESTAMP);
        when(resultSet.getBigDecimal("BALANCE")).thenReturn(BALANCE);
        when(resultSet.getString("MEMBERSHIP_TYPE")).thenReturn(MEMBERSHIP_TYPE);
        when(resultSet.getString("SOURCE_TYPE")).thenReturn(SOURCE_TYPE);
        when(resultSet.getInt("MONTHS_DURATION")).thenReturn(MONTHS_DURATION);
        when(resultSet.getString("PRODUCT_STATUS")).thenReturn(PRODUCT_STATUS);
        when(resultSet.getString("RECURRING_ID")).thenReturn(RECURRING_ID_01);
        when(resultSet.getTimestamp("MEMBERSHIP_TIMESTAMP")).thenReturn(MEMBERSHIP_TIMESTAMP);
        when(resultSet.getString("CURRENCY_CODE")).thenReturn(CURRENCY_CODE);
        when(resultSet.getBigDecimal("TOTAL_PRICE")).thenReturn(TOTAL_PRICE);
        when(resultSet.getBigDecimal("RENEWAL_PRICE")).thenReturn(RENEWAL_PRICE);
    }

    private void init(Binder binder) {
        binder.bind(MemberAccountEntityBuilder.class).toInstance(memberAccountEntityBuilder);
        binder.bind(MembershipRecurringEntityBuilder.class).toInstance(membershipRecurringEntityBuilder);
    }

    private void mockMembershipRecurringEntityBuilder() throws SQLException {
        when(membershipRecurringEntityBuilder.build(resultSet)).thenReturn(Optional.of(MEMBERSHIP_RECURRING_01)).thenReturn(Optional.of(MEMBERSHIP_RECURRING_02));
    }
}