package com.odigeo.product.membership.rest;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ServiceApplication extends Application {

    private final Set<Object> singletons = new HashSet<>();

    public ServiceApplication() {
        singletons.add(new ProductControllerV2());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}
