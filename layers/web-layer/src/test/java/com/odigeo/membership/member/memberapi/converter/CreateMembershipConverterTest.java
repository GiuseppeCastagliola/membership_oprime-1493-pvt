package com.odigeo.membership.member.memberapi.converter;

import com.google.gson.Gson;
import com.odigeo.membership.request.product.creation.CreateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreateNewMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import org.testng.annotations.Test;

import java.util.Map;

import static org.testng.Assert.assertEquals;

public class CreateMembershipConverterTest {

    private static final String REQUEST_TYPE = "requestType";
    private CreateMembershipConverter converter = new CreateMembershipConverter();
    private final Gson gson = new Gson();

    @Test
    public void testConvertWithCreateNewMembership() {
        CreateMembershipRequest request = new CreateNewMembershipRequest.Builder().build();
        String result = (String) converter.convertArgumentsToJson(request);
        Map map = gson.fromJson(result, Map.class);
        assertEquals(map.get(REQUEST_TYPE), "CreateNewMembershipRequest");
    }

    @Test
    public void testConvertWithPTCMembership() {
        CreateMembershipRequest request = new CreatePendingToCollectRequest.Builder().build();
        String result = (String) converter.convertArgumentsToJson(request);
        Map map = gson.fromJson(result, Map.class);
        assertEquals(map.get(REQUEST_TYPE), "CreatePendingToCollectRequest");
    }

}
