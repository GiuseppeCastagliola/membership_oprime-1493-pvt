package com.odigeo.membership.member.memberapi.v1.util;

import com.edreams.configuration.ConfigurationEngine;
import com.odigeo.membership.MemberAccount;
import com.odigeo.membership.MemberStatus;
import com.odigeo.membership.MembershipBuilder;
import com.odigeo.membership.MembershipRenewal;
import com.odigeo.membership.ProductStatus;
import com.odigeo.membership.response.MembershipInfo;
import org.apache.commons.lang.StringUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class MemberUserAreaServiceMappingTest {

    private static final String NAME = "Cloud";
    private static final String LAST_NAME = "Strife";
    private static final Long MEMBER_ACCOUNT_ID = 1L;
    private static final Long USER_ID = 91L;
    private static final Long MEMBERSHIP_ID = 8L;
    private static final String WEBSITE = "ES";
    private static final long BOOKING_SUBSCRIPTION_ID = 2010L;
    private static final long BOOKING_SUBSCRIPTION_ID_ZERO = 0L;

    private MemberAccount memberAccount;
    private List<com.odigeo.membership.Membership> membershipList = new ArrayList<>();

    @BeforeMethod
    public void init() {
        ConfigurationEngine.init();
        assembleTestObjects();
    }

    @Test
    public void testAsMembershipResponse() {
        MembershipInfo membershipInfoResponse = MemberUserAreaServiceMapping.asMembershipInfoResponse(memberAccount, BOOKING_SUBSCRIPTION_ID, null);
        assertNotNull(membershipInfoResponse);
        assertEquals(membershipInfoResponse.getBookingIdSubscription(), BOOKING_SUBSCRIPTION_ID);
    }

    @Test
    public void testAsMembershipResponseNullBookingId() {
        MembershipInfo membershipInfoResponse = MemberUserAreaServiceMapping.asMembershipInfoResponse(memberAccount, null, null);
        checkSubscriptionBookingZero(membershipInfoResponse);
    }

    @Test
    public void testMembershipLastStatusModificationDateIsReturnedWhenGiven() throws Exception {
        //Given
        final String expectedDate = "2019-01-04";
        final Date givenDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(expectedDate);
        //When
        MembershipInfo membershipInfoResponse = MemberUserAreaServiceMapping.asMembershipInfoResponse(memberAccount, 2L, givenDate);
        //Then
        assertEquals(membershipInfoResponse.getLastStatusModificationDate(), expectedDate);
    }

    @Test
    public void testMembershipLastStatusModificationDateIsEmptyWhenIsNotGiven() {
        //When
        MembershipInfo membershipInfoResponse = MemberUserAreaServiceMapping.asMembershipInfoResponse(memberAccount, 1L, null);
        //Then
        assertEquals(membershipInfoResponse.getLastStatusModificationDate(), StringUtils.EMPTY);
    }

    private void assembleTestObjects() {
        memberAccount = new MemberAccount(MEMBER_ACCOUNT_ID, USER_ID, NAME, LAST_NAME).setTimestamp(LocalDateTime.now());
        com.odigeo.membership.Membership membership = new MembershipBuilder().setId(MEMBERSHIP_ID).setWebsite(WEBSITE)
                .setStatus(MemberStatus.ACTIVATED).setMembershipRenewal(MembershipRenewal.ENABLED).setMemberAccountId(MEMBER_ACCOUNT_ID).setProductStatus(ProductStatus.CONTRACT).build();
        membershipList.add(membership);
        memberAccount.setMemberships(membershipList);
    }

    private void checkSubscriptionBookingZero(MembershipInfo membershipResponse) {
        assertNotNull(membershipResponse);
        assertEquals(membershipResponse.getBookingIdSubscription(), BOOKING_SUBSCRIPTION_ID_ZERO);
    }
}
