package com.odigeo.membership.member.bootstrap.betatracking.filters;

import com.edreams.configuration.ConfigurationEngine;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.ServletRequest;

import static org.testng.Assert.assertNotNull;

public class MembershipTrackingExecutionContextFilterTest {

    private MembershipTrackingExecutionContextFilter trackingFilter;
    @Mock
    ServletRequest servletRequest;

    @BeforeMethod
    public void setUp() {
        trackingFilter = new MembershipTrackingExecutionContextFilter();
    }

    @Test
    public void testGetFlowName() {
        assertNotNull(trackingFilter.getFlowName(servletRequest));
    }

    @Test
    public void testGetFlowNamespace() {
        assertNotNull(trackingFilter.getFlowNamespace());
    }

    @Test
    public void testGetFlowContextManager() {
        ConfigurationEngine.init();
        assertNotNull(trackingFilter.getFlowContextManager());
    }
}
