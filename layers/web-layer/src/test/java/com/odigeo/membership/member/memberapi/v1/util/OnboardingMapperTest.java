package com.odigeo.membership.member.memberapi.v1.util;

import com.odigeo.membership.onboarding.Onboarding;
import com.odigeo.membership.request.onboarding.OnboardingDevice;
import com.odigeo.membership.request.onboarding.OnboardingEvent;
import com.odigeo.membership.request.onboarding.OnboardingEventRequest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class OnboardingMapperTest {

    private final OnboardingMapper mapper = new OnboardingMapper();

    @Test
    public void testOnboardingDeviceMapped() {
        //Given
        OnboardingEventRequest requestWithApp = new OnboardingEventRequest.Builder().withDevice(OnboardingDevice.APP).withEvent(OnboardingEvent.COMPLETED).build();
        OnboardingEventRequest requestWithDesktop = new OnboardingEventRequest.Builder().withDevice(OnboardingDevice.DESKTOP).withEvent(OnboardingEvent.COMPLETED).build();
        OnboardingEventRequest requestWithMobile = new OnboardingEventRequest.Builder().withDevice(OnboardingDevice.MOBILE).withEvent(OnboardingEvent.COMPLETED).build();

        //When
        Onboarding mappedWithApp = mapper.map(requestWithApp);
        Onboarding mappedWithDesktop = mapper.map(requestWithDesktop);
        Onboarding mappedWithMobile = mapper.map(requestWithMobile);

        //Then
        assertEquals(mappedWithApp.getDevice(), com.odigeo.membership.onboarding.OnboardingDevice.APP);
        assertEquals(mappedWithDesktop.getDevice(), com.odigeo.membership.onboarding.OnboardingDevice.DESKTOP);
        assertEquals(mappedWithMobile.getDevice(), com.odigeo.membership.onboarding.OnboardingDevice.MOBILE);
    }

    @Test
    public void testOnboardingEventMapped() {
        //Given
        OnboardingEventRequest requestEventCompleted = new OnboardingEventRequest.Builder().withEvent(OnboardingEvent.COMPLETED).withDevice(OnboardingDevice.APP).build();

        //When
        Onboarding mappedEventCompleted = mapper.map(requestEventCompleted);

        //Then
        assertEquals(mappedEventCompleted.getEvent(), com.odigeo.membership.onboarding.OnboardingEvent.COMPLETED);
    }

    @Test
    public void testOnboardingMemberAccountMapped() {
        //Given
        Long memberAccountId = 832L;
        OnboardingEventRequest requestEventWithAccountId = new OnboardingEventRequest.Builder().withMemberAccountId(memberAccountId)
                .withEvent(OnboardingEvent.COMPLETED).withDevice(OnboardingDevice.APP).build();

        //When
        Onboarding mappedMemberAccount = mapper.map(requestEventWithAccountId);

        //Then
        assertEquals(mappedMemberAccount.getMemberAccountId(), memberAccountId);
    }
}