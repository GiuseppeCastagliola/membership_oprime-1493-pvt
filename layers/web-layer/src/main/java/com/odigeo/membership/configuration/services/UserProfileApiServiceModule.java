package com.odigeo.membership.configuration.services;

import com.edreams.configuration.ConfigurationEngine;
import com.google.inject.AbstractModule;
import com.odigeo.userapi.configuration.UserProfileApiSecurityConfiguration;
import com.odigeo.userprofiles.api.v1.UserServiceInternalManager;

public class UserProfileApiServiceModule extends AbstractModule {

    @Override
    protected void configure() {
        UserProfileApiSecurityConfiguration configuration = ConfigurationEngine.getInstance(UserProfileApiSecurityConfiguration.class);
        bind(UserServiceInternalManager.class).toInstance(new UserServiceInternalManager(configuration.getUser(), configuration.getPassword(), configuration.getUrl()));
    }
}
