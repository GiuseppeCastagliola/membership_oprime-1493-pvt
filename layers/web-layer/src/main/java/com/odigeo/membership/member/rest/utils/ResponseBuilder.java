package com.odigeo.membership.member.rest.utils;

import org.apache.commons.lang.StringUtils;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public final class ResponseBuilder {

    private static final List<String> STATES = Arrays.asList(HttpMethod.GET, HttpMethod.HEAD);

    private ResponseBuilder() {
    }

    public static Response buildResponse(final Object entity, final Request req) {
        return buildResponseBuilder(entity, req).build();
    }

    private static Response.ResponseBuilder buildResponseBuilder(final Object entity, final Request request) {
        if (entity == null || entity.hashCode() == 0) {
            return Response.noContent();
        }

        int eTagValue = entity.hashCode();
        Response.ResponseBuilder rb;
        final EntityTag eTag = new EntityTag(String.valueOf(eTagValue));
        if (STATES.contains(request.getMethod())) {
            rb = Optional.ofNullable(request.evaluatePreconditions(eTag))
                    .filter(ResponseBuilder::checkStatus)
                    .map(responseBuilder -> responseBuilder.entity(StringUtils.EMPTY))
                    .orElseGet(() -> Response.ok(entity));
        } else {
            rb = Response.ok(entity);
            if (HttpMethod.POST.equals(request.getMethod())) {
                rb.status(Response.Status.CREATED);
            }
        }
        rb.tag(eTag);
        return rb;
    }

    private static boolean checkStatus(final Response.ResponseBuilder responseBuilder) {
        int status = responseBuilder.build().getStatus();
        return status == 412 || status == 304;
    }
}
