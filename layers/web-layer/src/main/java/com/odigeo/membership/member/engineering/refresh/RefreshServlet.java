package com.odigeo.membership.member.engineering.refresh;

import com.edreams.persistance.cache.Cache;
import com.edreams.persistance.cache.impl.CacheRegistry;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class RefreshServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(RefreshServlet.class);

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        serveRequest(response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        serveRequest(response);
    }

    void serveRequest(HttpServletResponse response) throws IOException {
        LOGGER.info("Init RefreshServlet");
        CacheRegistry.getInstance().iterator().forEachRemaining(cache -> Optional.ofNullable(cache).ifPresent(Cache::clear));
        LOGGER.info("CacheRegistry cleaned");
        response.getWriter().print("REFRESH DONE");
        response.setStatus(HttpServletResponse.SC_OK);
    }
}
