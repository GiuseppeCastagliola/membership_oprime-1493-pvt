package com.odigeo.membership.world;

import cucumber.runtime.java.guice.ScenarioScoped;

@ScenarioScoped
public class PropertiesConfigurationWorld {

    private boolean updateResult;

    public boolean isUpdateResult() {
        return updateResult;
    }

    public void setUpdateResult(final boolean updateResult) {
        this.updateResult = updateResult;
    }
}
