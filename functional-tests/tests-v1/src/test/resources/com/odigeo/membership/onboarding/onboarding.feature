Feature: Test save onboarding service

  Scenario: save onboarding
    Given the next memberAccount stored in db:
      | memberAccountId | userId | firstName | lastNames |
      | 938             | 3436   | TEST      | TEST      |
    When the client request saveOnboarding to API
      | memberAccountId | event     | device |
      | 938             | COMPLETED | APP    |
    Then the onboarding is correctly saved
