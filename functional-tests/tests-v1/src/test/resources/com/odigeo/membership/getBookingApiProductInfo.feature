Feature: Test getBookingApiProductInfo service

  Background:
    Given the next membership product stored in db:
      | memberId | website     | status              | autoRenewal | memberAccountId | activationDate | expirationDate | balance | sourceType     | monthsDuration | productStatus |
      | 123      | ES          | ACTIVATED           | ENABLED     | 123             | 2017-07-06     | 2018-07-06     | 54.99   | FUNNEL_BOOKING | 12             | CONTRACT      |
    And the next membership product fee stored in db:
      |membershipId | amount | currency | feeType            |
      | 123         | 20.31  | EUR      | MEMBERSHIP_RENEWAL |

  Scenario: Checking getBookingApiProductInfo service
    When the product BookingApi information is requested for the product id 123
    Then the response to the membership product Booking Api request is:
      | membershipId | amount |
      | 123          | 20.31  |