package com.odigeo.converter;

class FunctionalTestConverterException extends RuntimeException {

    FunctionalTestConverterException(String message) {
        super(message);
    }
}
