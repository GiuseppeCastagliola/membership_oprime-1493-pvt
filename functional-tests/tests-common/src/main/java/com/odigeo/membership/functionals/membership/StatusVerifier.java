package com.odigeo.membership.functionals.membership;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static java.util.Objects.nonNull;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class StatusVerifier {

    private static final int ONE = 1;
    private static final String TODAY = "today";
    private static final String NEXT_MONTH = "nextMonth";

    private final String productStatus;
    private final String membershipStatus;
    private final BigDecimal balance;
    private final String activationDate;
    private final String expirationDate;

    public StatusVerifier(String productStatus, String membershipStatus, BigDecimal balance, String activationDate, String expirationDate) {
        this.productStatus = productStatus;
        this.membershipStatus = membershipStatus;
        this.balance = balance;
        this.activationDate = activationDate;
        this.expirationDate = expirationDate;
    }

    public void verifyStatus(String membershipStatus, String productStatus, BigDecimal balance, String activationDate, String expirationDate) {
        assertEquals(productStatus, this.productStatus);
        assertEquals(membershipStatus, this.membershipStatus);
        if (nonNull(this.balance)) {
            assertEquals(balance.setScale(2, RoundingMode.UP), this.balance.setScale(2, RoundingMode.UP));
        }
        if (nonNull(this.activationDate)) {
            assertTrue(activationDate.startsWith(TODAY.equals(this.activationDate) ? LocalDate.now().format(DateTimeFormatter.ISO_DATE) : this.activationDate));
        }
        if (nonNull(this.expirationDate)) {
            assertTrue(expirationDate.startsWith(NEXT_MONTH
                    .equals(this.expirationDate)
                    ? LocalDate.now().plusMonths(ONE).format(DateTimeFormatter.ISO_DATE)
                    : this.expirationDate));
        }
    }
}
