package com.odigeo.membership.functionals;

import org.apache.log4j.Logger;

public class InstallerTestSuiteEnvironment {

    private static final Logger logger = Logger.getLogger(InstallerTestSuiteEnvironment.class);

    public void install() {
        logger.info("Start install test suite environment");
        logger.info("End install environment");
    }

    public void uninstall() {
        logger.info("Start uninstall test suite environment");
        logger.info("End uninstall environment");
    }
}
